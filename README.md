## QA Engineer Challenge Guidelines

### Project Scope

Using the online check-in service guests can get the details about how to access the properties. Entering a valid booking reference and the last name of the reservation a guest is able to retrive the booking details and complete the online check-in.

Core features:

* Retrive the reservation details using the reference number and guest last name
* Capability to take/upload a picture of the passport that's a mandatory field for non-german nationalities  
* Mobile friendly web app
* Needs to be bilingual (german / english)

### Notes

* the service was adapted for the task and you can retrive a booking using any combination of reservation reference/last name
* do not consider any typos as a bug during the exercise.

## Tasks

#### Task #1: Writing Test Scenarios

As a QA Engineer you do not only test, you develop test scenarios too. 
Write test scenarios for the online check-in service.

#### Task #2: Finding Defects

As a QA Engineer, what do you do? Of course, can you find out any defects in the online check-in service?

* [https://limehome-qa-task.herokuapp.com](https://limehome-qa-task.herokuapp.com/)

#### Task #3: Defect Reporting

As a QA Engineer, create a defect/bug ticket. We would like to know how you would report just **one** of the defect you found during the test.

#### Task #4: Automation (Bonus)

As a QA Engineer, create a e2e automation who tests that the last name that the guest typed in the first screen is equal to the one shown in the form screen?
You can use any open source automation testing tool but we reccomend https://robotframework.org/

* Once the automation is up and running, fill up the date of birth and also change the nationality to Denmark(DK)

## Submission

Once you're done:

1. create a github/gitlab repository
2. write your answers in a file and commit it
3. (bonus done?) update the scripts in the repo and explain how to run it
4. If you were a user, what would you like to change to have a better online check-in experience?

## Final words

We're interested in your feedback, so do let us know what you thought of the task. Good​ ​luck​ ​and​ ​feel​ ​free​ ​to​ ​ask​ ​if​ ​there​ ​are​ ​any​ ​questions: devs@limehome.de

